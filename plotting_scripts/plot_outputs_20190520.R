#!/usr/bin/env Rscript

library(tidyverse)

dir = commandArgs(trailingOnly = TRUE)[[1]]


#' Read OSR output tables from all subdirectories into one dataframe
#'
#' @ param dir directory to read
#'
read_all_tabled_outputs <- function(dir = ".") {
	(tibble(
		filepath = list.files(
			dir,
			recursive = TRUE,
			pattern = "tabled_output.tab",
			full.names = TRUE))
	%>% mutate(
		data = map(
			filepath,
			read.table,
			header = TRUE,
			stringsAsFactors = FALSE,
			strip.white = TRUE))
	%>% unnest(data))
}

#' @param dat dataframe containing OSR tabled output
#' @param plus_value numeric value to assume for `depth_bottom` when deepest
#' 	layer is labeled like "90+"
#' @param varname name of root profile variable in `dat`
extract_root_profile <- function(dat,
								 plus_value = NA_integer_,
								 varname = "RootLengthProfile") {
	(dat
	%>% filter(grepl(varname, name))
	%>% extract(
		col = "name",
		into = c("depth_top", "depth_bottom"),
		regex = paste0(varname, "_(\\d+)-?(\\+|\\d+)$"),
		convert = FALSE,
		remove = TRUE)
	%>% mutate(
		depth_top = as.numeric(depth_top),
		depth_bottom = as.numeric(sub("\\+", plus_value, depth_bottom))))
}

dat <- (
	read_all_tabled_outputs(dir)
	%>% extract(
		col = filepath,
		into = c("geno", "replicate"),
		regex = paste0(dir, "/(.*)_(\\d+)/tabled_output.tab"),
		remove = TRUE,
		convert = TRUE))


prof <- extract_root_profile(dat, plus_value = 150) %>% select(-path, -rate)


# depth plots facetted by time
(ggplot(
	prof %>% filter(time %% 5 == 0),
	aes(x = depth_top, y = value, color = geno, group = paste(geno, replicate)))
+ geom_rect(
    data=data.frame(y1=-Inf, y2=Inf, x1=c(15, 20), x2=c(35, 30), alp = c(0.3, 0.4)),
    aes(xmin=x1, xmax=x2, ymin=y1, ymax=y2, alpha=alp), fill="lightgrey", inherit.aes=FALSE)
+ geom_line()
+ facet_wrap(~time, scales="free_x")
+ coord_flip()
+ scale_x_reverse()
+ theme_bw()
+ scale_alpha(range=c(0.3, 0.4), guide=FALSE))

#time plots facetted by depth
ggplot(prof, aes(x = time, y = value, color = geno, group = paste(geno, replicate))) +  geom_line()+facet_wrap(~depth_top)+theme_bw()


# mass of various pools at day 40
dat %>% filter(time == 40, grepl("Weight$", name)) %>% ggplot(aes(geno, value))+geom_point()+facet_wrap(~name, scales="free_y")+theme_bw()


# day 40 values of every param that's not constant across genotypes
# (excluding RootLengthProfile to save a few panels)
dat_varying <- (dat
	%>% filter(time==40, !grepl("RootLengthProfile", name))
	%>% group_by(name, path)
	%>% mutate(is_constant = length(unique(value)) == 1)
	%>% filter(!is_constant))

(ggplot(dat_varying,
	aes(geno, value, color=factor(replicate)))
	+ geom_point()
	+ facet_wrap(~name, scales="free_y")
	+ theme_bw())
# time courses of all the vars that vary in day 40
(ggplot(dat %>% filter(name %in% unique(dat_varying$name)),
	aes(time, value, color=geno, group=paste(geno, replicate)))
	+ geom_line()
	+ facet_wrap(~name, scales="free_y")
	+ theme_bw())

# carbon alllocation to various pools at day 40
dat %>% filter(time == 40, grepl("[Aa]llocation", name)) %>% ggplot(aes(geno, value, color=factor(replicate)))+geom_point()+facet_wrap(~name, scales="free_y")+theme_bw()
# ... and as timecourse
dat %>% filter(grepl("[Aa]llocation", name)) %>% ggplot(aes(time, value, color=geno, group=paste(geno,replicate)))+geom_line()+facet_wrap(~name, scales="free_y")+theme_bw()





## reading class-specific root profiles from VTPs
## with extra bells and whistles to make a general-purpose
## read_vtp that includes root IDs

# Assumes array was read with XML::xmlToList
# ==> i.e. contents are in $text, XML attributes are in $.attrs
read_DataArray <- function(arr){
	stopifnot(arr$.attrs[["format"]] == "ascii")
	n_comp <- arr$.attrs[["NumberOfComponents"]]

	if(n_comp == 3) {
		# Assumption: 3 components always means coordinate
		return(
			utils::read.table(
				text = arr$text,
				col.names = paste0(arr$.attrs[["Name"]], "_", c("x", "y", "z")),
				colClasses = "numeric"))
	} else if (n_comp == 1) {
		return(
			stats::setNames(
				tibble::tibble(scan(text = arr$text, quiet = TRUE)),
				arr$.attrs[["Name"]]))
	} else {
		stop("Don't know how to read an array with ", n_comp, " components")
	}
}

# takes two vectors:
# * `connectivity`
#	- vector with n_points entries
#	- values are zero-based indices into the point positions list
# 	- for OSR output this is probably the sequence 0:(n_points - 1),
#		but other VTP files may reuse points or write them in a different order
# * `offsets`
#	- vector with n_roots entries
#	- values are zero-based offsets into the connectivity list
#	- each index points to the last point for that root,
#		so the i'th root passes through the coordinates given by
#		`points[(connectivity[[offsets[[i-1]] + 1]] +1):(connectivity[[offsets[[i]] + 1]] + 1)]`
# returns a tibble with `n_points` rows and three columns:
# * root_number (range 1:n_roots),
# * node_in_root (sequential within each root)
# * rowid (sequential and hopefully 1:n_points)
match_roots <- function(con, off) {
	offset_pairs = list(
		starti = lag(off, default = 0) + 1,
		endi = off)

	con_byroot <- pmap(
		offset_pairs,
		function(starti, endi) { con[starti:endi] })

	imap_dfr(
		con_byroot,
		~tibble(
			root_number = rep(.y, length(.x)),
			node_in_root = seq_along(.x),
			rowid = .x + 1))
}

read_vtp <- function(path) {
	x <- XML::xmlToList(XML::xmlParse(path))
	xpts <- (
		bind_cols(
			read_DataArray(x$PolyData$Piece$Points$DataArray),
			map_dfc(x$PolyData$Piece$PointData, read_DataArray))
		%>% rowid_to_column())
	xlines <- flatten(map(x$PolyData$Piece$Lines, read_DataArray))
	root_info <- match_roots(xlines$connectivity, xlines$offsets)

	left_join(xpts, root_info, by="rowid")
}

all_40 <- (
	tibble(filepath = list.files("out_20190518", recursive=TRUE, full.names=TRUE, pattern = "roots040.00.vtp"))
	%>% mutate(data = map(filepath, read_vtp))
	%>% extract(
		filepath,
		c("geno", "replicate"),
		".*20190518/(.*)_(\\d)/roots040.00.vtp")
	%>% unnest(data)
	%>% mutate(
		depth_layer = cut(Position_y, seq(10, -200, -10)),
		rootClassName = recode(
			rootClassID,
			`0` = "growthpoint?", #todo confirm this
			`97` = "hypocotyl",
			`98` = "lateral",
			`99` = "seminal",
			`100` = "primary",
			`101` = "nodal",
			`102` = "brace"))
	%>% extract(
		depth_layer,
		c("depth_top", "depth_bottom"),
		"\\((.*),(.*)\\]",
		convert = TRUE))


(all_40
	%>% filter(rootClassName != "growthpoint?")
	%>% group_by(geno, replicate, rootClassName, depth_top)
	%>% summarize(len = sum(rootSegmentLength))
	%>% ggplot(aes(depth_top, len, color=geno))
		+ geom_rect( # shows high-density zone
			data = data.frame(
				y1 = -Inf, y2 = Inf,
				x1 = c(-15, -20), x2 = c(-35, -30),
				opacity = c(0.3, 0.4)),
			aes(xmin = x1, xmax = x2, ymin = y1, ymax = y2, alpha = opacity),
			fill = "lightgrey",
			inherit.aes = FALSE)
		+ scale_alpha(range = c(0.3, 0.4), guide = FALSE)
		+ geom_smooth()
		+ geom_jitter(width = 1, height = 0)
		+ facet_wrap(~rootClassName, scales = "free_x")
		+ coord_flip()
		+ theme_bw())
