# Modeled root response to soil impedance implies axial strength is key for increasing root depth

## Intro

* soil impedance restricts root growth and causes $BIG_NUMBER of lost crop yield every year, mostly by compounding drought stress and limiting nutrient uptake.
* Breeding for deeper roots has been suggested as a strategy to alleviate both drought and nutrient stress and may also increase soil C storage, but in general soil hardness becomes increasingly limiting as depth increases. Any breeding program that wants to increase root depth will necessarily need to grapple with the effects of hardness.
* Direct measurements of root architecture are laborious and destructive, so for breeding programs we want proxy markers rather than try to phenotype the roots directly. This is great, if our proxies are good enough!
* Models of RSA are very useful tools for understanding root growth, for evaluating the utility of proposed proxy markers, and for generating hypotheses about what phenes might produce a desired outcome. But model predictions are only as good as their inputs! Most models currently ignore soil hardness, which limits their utility for understanding rooting depth.
* We have added physics-based representation of soil hardness and of root responses to hardness to the structural-functional plant root model OpenSimRoot. Soil hardness is calculated from measurable parameters specific to the soil type, position in the soil profile, and current water status. Root growth responses can be specified by the user to match experimental data for a particular crop or to test competing hypotheses about the functional form of impedance responses. We demonstrate the utlitiy of this module through an in silico experiment where we demonstrate that the soil penetration ability of axial roots is key to deep rooting of a simulated maize crop, even if resource tradeoffs requires weakening lateral roots to do so. We show that this holds true across a range of soil compaction types and water statuses, implying that this is likely to hold generally true for monocots growing under most most field conditions.

# Materials and methods

* See Postma 2017 for basic description of OpenSimRoot
* Impedance function comes from Gao et al. 2016.
	- basic form: [eqn here]
	- Translation: contains terms for bulk density, porosity (affects elastic modulus beyond its effect on density), stress from overlying soil layers, water status.
	- Implementation in OSR uses standard coefficients that should work for most soils. Required input parameters are bulk density for all relevant depths, porosity if available (if not provided, will be estimated from bulk density assuming particle density = quartz = 2.65). Hydraulic parameters are taken from OSR's existing water flow simulations.
	- For efficiency, currently assumes that bulk density only varies with depth (i.e. horizontal heterogeneity is not yet considered) and is constant over time. Future work could relax these assumptions to allow exploration of more detailed soil structures (macropores, wheel-track compaction, etc) and of feedbacks over time (e.g. simulating the loosening of compacted layers by the roots of cover crops).
* Root responses to soil hardness are designed to be easily reconfigurable
	- For the in silico experiment presented here, we use a scaled an inverted Michaelis-Menten function with a user-set Km, such that a root's potential growth rate relative to unimpeded conditions is 1 when soil hardness is 0 kPa, drops to 0.5 (i.e. compaction has cut growth rate in half) at the user-specified Km, and asymptotes to 0 as soil hardness approaches infinity.
	- Under typical field conditions with bulk density in the range of 1.1 to 1.6 g/cm3 and soil water contents above the wilting point, the convolved output of the soil hardness and relative growth functions  is close to linear, consistent with published observations of quasi-linear reductions in root growth rate as bulk density increases.
	- In addition to affecting elongation rate, a scaling parameter can be set to increase the root diameter in proportion to its reduction in growth rate. Future work should explore an alternate formulation where diameter directly influences penetration ability, but this is not yet implemented.
* Validation against field data from a compaction experiment
	- Field data come from maize lines growth with and without soil compaction in PA and AZ, as previously presented in Vanhees et al (2020).
	- Ran model with and without impedance module, with soil and environment conditions set to match each site.
* In silico experiment:
	- Simulated maize varieties differing only in Km of their elongation respomnse to soil penetration resistance: axial roots and lateral roots either weak (Km = 1000 kPa) or strong (4000 kPa).
	- Simulated 40 days of growth under wet conditions (soil initially at field capacity, precipitation schedule from summer [YYYY] in Rock Springs PA) or dry (soil begins at [give water potentials here], no rain for whole simulation)
	- Water flow simulated with OSR's existing implementation of the SWMS3D model, plant uptake calculated by Doussan method [with Ernst's recent fixes -- cite?].
	- Note that water flow simulations might not account for the imposed soil compaction treatments -- check this! If not, may need to manually calculate and provide as depth tables.
	- N uptake simulated but with sufficient N to be unrestrictive for all conditions (right?)
	- Soil hardness used 3 scenarios: uniform soft (1.0 g cm3), linear depth gradient (1.0 at surface to 1.8 at 100 cm), plowpan (1.0 0-15, 1.6 20-30, 1.0 35-150, linear gradients at top and bottom of hard layer).
	- 6 replicates of each to examine effects of random variation in root growth angle, extension rate, etc. Note that this randomness does not include any variation in weather, soil conditions, sunshine, etc.
	- Simulations were run on the PSU computing cluster. All parameter files, run scripts, and code needed to compiled our version of OpenSimRoot are available at <archive link>. The imnpedance module has been added to the public version of OpenSimRoot, which can be freely obtained from rootmodels.gitlab.com/OpenSimRoot.

# Results

* Validation: Simulations with no impedance module had root biomass and depth distribution comparable to uncompacted treatments but overpredicted root depth of compacted plants. Simulations with impedance module were comparable to both. [figure: compacted and uncompacted D75 and D75c from field and from simulation]
* Across all soil compactions and water conditions, simulations with strong axial roots rooted deeper than those with weak axials. Differences in lateral root strength produced differences in overall root length within a layer, but did not change rooting depth. [root profile figure]
* the uniform soft soil treatment had the deepest rooting profile, and the gradient compaction scheme limited root growth more than the plowpan did. Plowpan simulations shows sharp reductions in root length within the hard layer, but both lateral and axial root length recovered quickly below the plowpan.
* Plowpan treatment shows small compensatory growth in surface layer! In dry conditions, this causes severe drying of the surface layer that made some models crash due to numerical errors.
* Plants simulated unders dry conditions were slightly smaller overall than plants grown under wet conditions, and the differences were slightly larger in strong-axial than weak-axial plants.
* Plants simulated in gradient-compacted soil took up less N than others, with no differences between phenotypes. Probably because their maximum rooting depth is much shallower and they are unable to take up deep-leached N.
* Differences between phenotypes and environements in plant size and water and nutrient uptake were associated more closely with differences in root *mass* than with total root *length*, or in other words weak lateral roots showed a substantially lower *total root system length* but paid few consequences in biomass or resource use, while loss of mass from axial roots impacted the plant strongly.

# Discussion

* We have implemented a soil hardness simulator in OpenSimRoot that is based on well-characterized soil physics and can be easily parameterized to create realistic representations of soils that reflect real-world agronomic challenges.
* Other root models take different approaches to accounting for hard soils. Brief comparison here, upshot that most build it implicitly into their treatment of growth rates rather than as a physical mechanism.
* Future technical work:
	- feedbacks between hardness and plant architecture, e.g. account for anatomical differences such as diameter and ability to alter local environment such as through amount and composition of exudates
	- explicit representation of 3D heterogeneity (macropores etc). Theoretically possible to do with our existing framework if you specify bulk density for every vozel, but have not tested this.
* The simulator is available to the public as part of OpenSimRoot and we hope you use it.
* The results of our in silico experiment suggest that when breeding for deep-rooted crops, if a tradeoff exists between strength of axial and lateral roots it is likely more beneficial to take the axial-strength side of the tradeoff.
