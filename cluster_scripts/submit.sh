#!/bin/bash

#PBS -A open
#PBS -l nodes=1:ppn=1
#PBS -l walltime=12:0:00
#PBS -l pmem=10mb
#PBS -l epilogue=postjob.sh
#PBS -j oe
set -e



# file from which to read model names, one per line
MODEL_LIST="model_files.txt"
# max jobs to have queued+running at once. For ACI open queue, not more than 100
QUEUE_LIMIT=100
# how often to check for queue openings, in seconds.
CHECK_INTERVAL=300

cd ${PBS_O_WORKDIR}

n=$(wc -l < ${MODEL_LIST})
i=1
while [ ${i} -le ${n} ]; do

	n_in_queue=$(
		qstat -u $(whoami) -t | # get job list
		grep -E '^[[:digit:]]+' | # drop headers & spacer lines
		awk '{print $10}' | # extract status column
		grep -v '^[CE]$' | # remove complete/exited jobs (they don't count toward queue limit)
		wc -w) # count what's left
	slots_free=$((QUEUE_LIMIT - n_in_queue))

	if [ ${slots_free} -gt 0 ]; then
		for j in `seq ${slots_free}`; do
			nextmodel=`tail -n+${i} ${MODEL_LIST} | head -n1`
			qsub \
				-N `basename ${nextmodel}` \
				-v file=${nextmodel} \
				run_model.sh \
			&& i=$((i + 1))
			if [ ${i} -gt ${n} ]; then
				exit 0
			fi
		done
	fi

	sleep ${CHECK_INTERVAL}
done
