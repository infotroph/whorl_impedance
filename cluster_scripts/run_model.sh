#!/bin/bash

#PBS -A open
#PBS -l nodes=1:ppn=1
#PBS -l walltime=12:00:00
#PBS -l pmem=12gb
#PBS -l epilogue=postjob.sh
#PBS -j oe

set -e

# When called directly: `./run_model.sh path/to/infile.xml`
# When called by qsub: `qsub [q_args] -v 'file=path/to/infile.xml' run_model.sh`
if [ -z ${file} ]; then
	file=$1
fi
if [ -z ${PBS_O_WORKDIR} ]; then
	PBS_O_WORKDIR=$(pwd)
fi

PARAM_FILE=$(basename ${file})
RUN_DIR=$(dirname ${file})

OPENSIMROOT=${PBS_O_WORKDIR}/OSR_precalcnetstress_ecdcb0a9

BATCH_LOG=${PBS_O_WORKDIR}/batch_log.txt

TIME=`date +%s`
cd ${PBS_O_WORKDIR}/${RUN_DIR}
echo "${PARAM_FILE} start ${TIME}" >> ${BATCH_LOG}

(time ${OPENSIMROOT} ${PARAM_FILE}) >log_${TIME}.txt 2>&1

ENDTIME=`date +%s`
echo "${PARAM_FILE} done ${ENDTIME} ("$((ENDTIME - TIME))" sec)" >> ${BATCH_LOG}
