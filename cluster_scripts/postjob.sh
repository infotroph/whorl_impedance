#!/bin/bash

# Postrun script to report exit status and usage statistics of Torque jobs
# Usage: qsub -l epilogue=postjob.sh [other args] yourscript.sh
# Writes to stdout of the job that called it ==> output will be at end of your job's log file
#
# NB: Torque is picky about file permissions for epilogue scripts.
# This file must be executable by owner and NOT writable by either group or world,
# else Torque will silently(!) skip running it.

echo "Postrun report:"
echo "Job ID: $1"
#echo "User ID: $2"
#echo "Group ID: $3"
echo "Job Name: $4"
#echo "Session ID: $5"
echo "Resource List: $6"
echo "Resources Used: $7"
#echo "Queue Name: $8"
#echo "Account String: $9"
echo "Exit code: ${10}"
#echo "script location: ${11}"
echo ""

# qstat isn't updated to show completion status until Torque recieves final update from compute node
# If output still says "running", extend this wait
# but epilogue will be terminated after five minutes runtime, so don't extend too long
sleep 60
echo '===qstat output at exit time==='
qstat -f1 $1
exit 0
