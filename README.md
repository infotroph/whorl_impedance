
# Exploring whorl-specific effects of soil impedance on simulated maize growth

## 2019-05-16

Branching off somewhat from the impedance-angle project to think about models where the growth response to soil impedance differs from one root class to another.
We might invoke a few different underlying drivers for this:

* From a purely mechanical standpoint, roots with different diameters and cortical structures might differ in their strength and therefore ability to extend against soil pressure.
* If ability to penetrate hard soil is costly and trades off with other root functions (eg resource uptake rate), the developmental program might favor expression during growth stages more likely to encounter high impedance.
* Alternately there might be feedbacks between impedance and signal transduction that vary during development.
* Also want to consider difference between differences based on *plant* age, differences based on *root* age, and differences based on root *class*.

### Model implementation

OpenSimRoot's impedance module uses soil bulk density, van Genuchten water characteristic parameters, current water status, and profile position to calculate soil physical penetration resistance in kPa.

The conversion from physical impedance to growth response is then computed as a scaling factor that varies from 1 (at 0 kPa) to to 0 (at infinity) according to a negative Michaelis-Menten function whose Km can be interpreted as "the penetration resistance at which growth is reduced by 50%".

This scalar is then applied to the longitudinal growth rate of the root as a multiplier, and to the diameter of the root as a divisor *after scaling by a fixed exponent*: `lengthRate = lengthRate * impedanceScalar`, but `diameter = diameter / (impedanceScalar^exp)`. Note that higher impedance always implies slower root extension, but diameter could either increase or decrease depending whether `exp` is positive or negative. If no exponent is set, the default of 0.5 can be interpreted as "diameter increase offsets length decrease, so there is not change in total root volume."

In my previous tests of the impedance model, I always left Km hard-coded in the OSR C++ code and included the diameter-scaling exponent as a child of the impedance-scaling calculator object. However, it's convenient to specify the impedance-scaling calculator in the template section, so to achieve different parameters for each root class I need to move them to a more configurable location, so fixed this in the OSR code:

* Commit 9e4c870 of the OSR `impedance` branch implements reading the Km from the XML file in the form of a parameter named `rootTypeParameters/<rootClassName>/soilImpedanceFor50PercentGrowthSlowdown`. If this parameter doesn't exist, the growthImpedance object will look for its own child named `soilImpedanceFor50PercentGrowthSlowdown` before giving up.
* Commit 1278490 of the OSR `impedance` branch allows reading of the diameter scaling exponent from a parameter named `rootTypeParameters/<rootClassName>/scalingExponentForRootDiameterIncreaseFromImpedance`. If this doesn't exist, the diameterImpedance object will look for its own child named `scalingExponentForRootDiameterIncreaseFromImpedance` (as was previously the default) before giving up.


For initial testing, I'll use a simple hardpan soil with sufficient moisture, and vary parameters to produce visible differences in hardpan response between root classes. Once configured, I'll then test the same parameter set on a non-hardpan soil subjected to hardening by progressive drydown.

Initial parameter file: Copied `model.base.xml` from commit fd1a49d7 of my existing `impedance_angle` project. This file uses the `maize-aerenchyma` genotype parameters and Wageningen environmental parameters, with Hopmans water model enabled and no N model.

Saved `model_base.xml` and this README  as initial commit to a new repository.


### Prepare base model for whorl-specific parameters

* Added `<SimulaConstant name="soilImpedenceFor50PercentGrowthSlowdown" unit="kPa" type="double">2000</SimulaConstant>` to the `rootTypeParameters` sections of all 13 root classes. This leaves them all initially at default sensitivity to impedance -- I will adjust individual classes in a later step.
* Added `<SimulaConstant name="scalingExponentForRootDiameterIncreaseFromImpedance" type="double">0.4</SimulaConstant>` to all root classes, removed it from all 7 existing instances of `rootGrowthImpedanceRateMultiplier`.
* Removed all 7 `<SimulaLink linksName="../rootGrowthImpedance" name="impedanceGrowthFactor" prettyName="impedance growth factor"/>` that were existing siblings of `rootGrowthImpedance`. This was a hack to get impedance in the VTU output: Objects with children are skipped by default, so we created a childless link to work around that. Now that we've removed the children from rootGrowthImpedance, it shows up by default.

## 2019-05-17

### Set up whorl modifications

The basic approach here is that each model's parameterization uses `model_base.xml` as its baseline, then we apply a set of changes specified in `parameter_changes.csv`, which has four columns:

* `model_id` is a name for this parameter set (Conceptually, the variety name plus any treatment IDs).
	All rows of the CSV that have the same `model_id` will be processed as a group to produce a single XML file of the same name.

* `attribute` specifies what XML attribute to change, or "text" to change its value:
	- To change the value of `<foo name="bar" unit="cm">3</foo>` from 3 to 4, use path="bar", attribute="text", value="4".
	- To change its "units" attribute from cm to km, use path="bar", attribute="unit", value="km".

	For this experiment I expect all parameters to be "text", but including this column from the get-go will make it easy to adjust attributes as needed.
	E.g. to change the sampling range of parameters defined as `SimulaStochastic` objects, we may want to change their `minimum` and `maximum` attributes.

* `path` is the slash-separated path to the parameter(s) to be changed.
	Partial paths are OK if they're unique enough for your purpose:
	`origin/plants/maize/plantPosition/primaryRoot/growthpoint/rootDiameter` can always be abbreviated to `primaryRoot/growthpoint/rootDiameter` and still only change one parameter,
	but further shortening to `growthpoint/rootDiameter` will change diameters in the growthpoint of every root class,
	and specifiying just `rootDiameter` selects every root diameter in the whole model (i.e. both growthpoints and datapoints).

* `value` gives the new value for the parameter to take. It will be interpreted as text and inserted verbatim into the XML, so you can include e.g. whole SimulaTables as "0 1 2 2 100 0".
* Any other columns in the file are ignored, but can be used to e.g. cite sources for parameter values or to keep notes on unit conversions.

The process of generating model files is handled by a script named `set_up_models.R`.
When run, it will read `model_base.xml` and `parameter_changes.csv`, generate one parameter set for each `model_id` specified in `parameter_changes.csv`, then create as many replicate runs as requested.
Each replicate will give different values to any randomly generated parameters -- which currently consist only of `randomNumberGeneratorSeed`, but may eventually include environmental conditions.

(This raises an unresolved question: When there are multiple random parameters, how to specify them in a way that's easy to find and understand?
For previous experiments the randomization has been hidden inside the model setup script, which works when just changing the RNG seed but feels black-box-y when there are multiple parameters. Need to consider this more, but in the meantime I'll stick with what I did before.)

Committed first version of `parameter_changes.csv` defining five models that differ only in impedance susceptibility:
Models `all1k`, `all2k`, `all4k` set Km (AKA `soilImpedenceFor50PercentGrowthSlowdown`) of every root class to 1000, 2000, 4000 kPa respectively.
Model `ax1k_lat4k` sets Km for axial roots (`braceroots`, `braceroots2`, `hypocotyl`, `nodalroots`, `nodalroots2`, `nodalroots3`, `nodalroots4`, `primaryRoot`, `seminal`) to 1000 and Km for lateral roots (`finelateral`, `finelateral2`, `lateral`, `lateralOfCrownRoots`) to 4000, while model `ax4k_lat1k` does the opposite.
All five models initially use a soil hardness profile that could be described as "extreme plowpan": 1.0 g/cm3 from the surface to 15 cm depth, then linearly rising to 1.6 g/cm3 at 20-30 cm, dropping back to 1.0 by 35 cm, staying 1.0 from 35 to 150 cm.
For the moment `scalingExponentForRootDiameterIncreaseFromImpedance` stays unchanged and is therefore 0.4 as specified in the baseline model.


## 2019-05-18

First version of `set_up_runs.R` is finished. ACI cluster is down for the weekend, so rather than produce an ACI run script I currently have it writing a simple Bash script that runs models locally, up to 4 at a time using `parallel`. Using OpenSimRoot binary `OSR_impedrbih_0fd98410`, which includes both the whorl-specific impedance changes I detailed on 2019-05-16 and also adds some miscellaneous fixes from upstream, merged in from my `report-build-in-help` branch.

Test run: five replicates of all five models, with only `randomNumberGeneratorSeed` differing between replicates. Output directory is very imaginatively named `out_20190518`:

```
./set_up_runs.R out_20190518 5
cd out_20190518
chmod +x run_parallel.sh
chmod +x run_model.sh
caffeinate -i ./run_parallel.sh
```

Models ran for between 4255 and 8008 sec each, and whole batch took about 12.5 hours (17:40 2019-05-18 to 06:09 2019-05-19), with only one model running for the first hour or so (until I found and fixed a typo in the parallel script) and then four running at once after that.


## 2019-05-20

First pass at plotting outputs from 2019-05-18 run. Plotting code is in `plot_outputs_20190520.R`, including some data-reading functions that I hope will be reusable -- consider adding these to `simrootR`?

Most plots saved only as PDFs in Box, not committed here. Also saved Paraview screenshots comparing root system appearance of replicate 1 from each genotype.

Showed these to JPL, agreed that the most interesting results are:

* No surprise that the all4k plant has the most roots, but interesting that `ax4k_lat1k` comes so close, especially since so much of the total root length is in laterals!
* Comparing day 40 genotype averages of `carbonReserves` against `plantDryWeight`, looks like differences in reserve mass account near-entirely for differences in plant mass. This is cool! But JPL asks whether `carbonReserves` is included in the calculation of plant weight or not, and I'm not sure. If it isn't, then these plants are really close to identical in size, we just stopped counting some of the carbon.
* It looks as if the more-impeded genotypes may increase their lateral root length in the soft soil above the impeded layer. Can we verify that? Yes, but I'll need to dig into VTP files to extract individual root data to do it.

Next steps: undecided, but 3 good options:

* Start exploring water limitation
* Turn on N module and see what difference it makes (but then I'll have to resolve the asymmetric-plant problem I encountered in Hannah and Jen's NRN project)
* Move on to look at same genotypes in a depth-gradient hardness scenario


## 2019-05-21

Following up on JPL question about lateral length in top layers: pulled individual-root data from all day 40 VTP files, plotted by depth and root class. file-reading and plotting code added to `plot__outputs_2190520.R`, including some elaborate extra steps to convert VTP "Lines" entries into root identifiers, which I then did not actually use in the final plots. Will probably add this code to `simrootR` for future use.


#2019-06-03

Have fixed an OSR issues with plant asymmetry under low-N conditions, so now turning on N model for these simulations without worrying that it will complicate result interpretation. For initial runs, I'm leaving N supply high and expect N uptake to track closely with biomass, but will be interesting to consider when evaluating root depth implications of hardness.

But first: to avoid needing to add parameters in multiple places, a simplification I should have done a while ago: The existing `plants/maize/plantPosition` contains a full instantiation of the maize plant, with most of its 600ish lines being duplicates of parameters already specified in the corresponding template section. Added attribute `objectGenerator="seedling"` to `plants/maize` so that OSR will regenerate its contents from templates, then deleted nearly all child objects leaving only

```
<SimulaBase name="maize" prettyName="maize" unit="noUnit" objectGenerator="seedling">
	<SimulaConstant name="plantPosition" prettyName="plant position" type="Coordinate" unit="cm">
		0	-2	0
	</SimulaConstant>
	<SimulaConstant name="plantType" prettyName="plant type" type="string" unit="noUnit">
		maize-aerenchyma
	</SimulaConstant>
	<SimulaConstant name="plantingTime" prettyName="planting time" type="double" unit="day">
		0
	</SimulaConstant>
</SimulaBase>
```

Now ready to add N model:

* Copied all parameters from OSR repository version of N template `plantTemplate.IncludeNitrate3D.xml` into their matching paths in `model_base.xml`
* test run with patched binary `OSR_impedrbih_2cf16de1` runs successfully, though with more C balance warnings from the N-enabled model and slightly faster (2:29, no-N model took 2:52). Plant weight is about half that of no-N model (around 34 g without N, 18 with). I guess these default conditions are actually somewhat N stressed!
* Probably the Correct fix for N limitation while increasing ease of comparison to my previous work would be to switch from Wageningen environment to Rock Springs.
* But instead, today I'll do the Easy fix: added a multiplier to `/environment/soil/nitrate/concentration` and set it to 2.0 for initial runs.


## 2019-06-04

### New system for specifying parameter changes

Next I'd like to add a depth-gradient hardness treatment to the experiment, but it's annoying that with my current system for specifying conditions in `parameter_changes.csv` this would require doubling the number of lines in the file:  I'd need to re-specify all parameters for every model, even though the root penetration resistances of every root whorl of a gradient-hardness model remain the same as in their plowpan counterpart. Instead, I'll try a new system that adds a level of conceptual complexity but may be easier to edit and to read:

* As before, every *experiment* is a collection of one or more replicate simulations from a defined set of *model parameterizations*, which I'll shorten to "model" for the rest of this description. All the models in a given experiment are constructed by starting from one baseline parameterization named `model_base.xml`, which defines the canonical value for every parameter not varied in the experiment, then applying model-specific changes.

* The differences between models are created by varying one or more experimental *factors* across a defined set of *levels*, and each level is created by changing some defined set of *parameters*. Therefore we encapsulate these definitions in two files: First `factors_parameters.csv` rolls up sets of parameters into named factor levels, each of which might contain anywhere from one parameter (e.g. soil hardness) up to arbitrarily many parameters (e.g. changing from genotype A to genotype B might require changes in growth rate, root angle, heat sensitivity, drought resistance...). Second, `models_factors.csv` rolls up combinations of multiple factors into named models, each of which uses only a single level of each factor.

* `factors_parameters.csv` keeps most of the columns previously stored in `parameter_changes.csv`, but now only needs one row per parameter changed per factor level. So the levels for a 2x2 design varying water content and soil hardness might look like:
```
factor_name, factor_level, attribute, path, value
water, ten, text, /soil/water/SaturatedConductivity, 10
water, ten, text, /soil/water/residualWaterContent, 0.067
water, fifty, text, /soil/water/SaturatedConductivity, 50
water, fifty, text, /soil/water/residualWaterContent, 0.087
hardness, low, text, /soil/bulkDensity, 0 1.0 -1000 1.0
hardness, high, text, /soil/bulkDensity, 0 1.8 -1000 1.8
```
All parameters not listed for a given factor level will be left at their baseline values.

* `models_factors.csv` has one row for each model to be run and one column for each factor that will be varied:
```
model_id, water, hardess
soft_dry, ten, low
soft_wet, fifty, low
hard_dry, ten, high
hard_wet, fifty, high
```
Note that this does not enforce the design being factorial, complete, balanced, or even that the factors be related in any way (but in that case good luck interpreting the output). To "skip" a factor for a given model (i.e. use baseline values for all that factor's parameters), use an NA in that cell:
```
model_id,photosynthesis,cheese_intake
pure_baseline, NA, NA
hi_ps, high, NA
low_ps, low, NA
cheesy, NA, extreme
```

### Gradient hardness

Now I'm ready to add soil hardness treatments using this new system:

* copied the unique lines from `parameters_changes.csv` into new file `factors_parameters.csv`, divided them into three factors: `axial_penetration` with levels `1k` and `4k`, `lateral_penetration` with levels `1k` and `4k`, and `bulk_density` with level `gradient`.
* added a line for a new `gradient` level of `bulk_density`, rising linearly from 1.0 g/cm^3 at the surface to 1.8 g/cm^3 at 100 cm depth, then constant 1.8 below that.
* both penetration factors vary only `soilImpedanceFor50PercentGrowthSlowdown`; `axial_penetration` varies it in all of the axial root classes (`braceroots`, `braceroots2`, all 4 `nodalroots`, `primaryRoot`, `seminal`) but does not change it in laterals, `lateral_penetration` varies it in the four lateral classes but not axial.
* removed all entries for `hypocotyl/soilImpedanceFor50PercentGrowthSlowdown` -- was previously considering it axial, but instead let's just leave it at baseline values.
* set up new `models_factors.csv` to define 15 treatments: factorial interaction of the axial and lateral penetration abilities with three bulk densities (gradient, plowpan, and treating the baseline uniform 1.0 as a third level without defining it in the levels file), plus baseline penetration abilities (equal to the previous "all2k" model) with each of the three bulk densities
* updated `set_up_models.R` to read both design files, combined them into a full set of parameter changes, and write the result to `[output_dir]/parameter_changes.csv` for easy reference later.


## 2019-06-05

Adapting run scripts in preparation for sending to the cluster. Updated `run_model.sh` to use binary `OSR_impedrbih_2cf16de1` (which has radial angle patch), added new script `submit.sh` (which monitors queue status and submits models as slots become available for them), and altered `set_up_runs.R` to write a plain text list of models to be run (named `model_files.txt`) rather than a bash script. The run setup process is now:

* update `model_base.xml` as needed to define the baseline conditions for the whole run.
* define treatment factors by specifying the parameters to be updated for each factor level in `factors_parameters.csv`, as described yesterday.
* define model parameterizations by specifying factor levels for each model in `models_factors.csv`, as described yesterday.
* run `./set_up_models.R <outdir> <n_reps>`, where `<outdir>` is the name of the directory to put the results in and `<n_reps>` is the number of times each model parameterization should be repeated with different values for its random parameters.
* After running `set_up_models.R`, the output dir will contain:
	- subdirectories for all ((n reps) * (n rows in `models_factors.csv`)) models, named as `<model_id>_<rep>`
	- `parameter_changes.csv`, containing a complete list of the parameters changed for each model
	- `model_files.txt`, containing a list of the paths to every input file in the run, with one path per line
	- Three files copied verbatim from the parent dierctory: the OSR binary, script to run individual models `run_model.sh`, and script to feed models into the ACI queue `submit.sh`.
* To launch...
	- ...any one model, `cd <outdir>` and then run `./run_model.sh <path/to/model.xml`
	- ...the entire run on the PSU cluster, copy `<outdir>` into your work directory on the cluster, then log in to ACI-b, `cd ~/work/<outdir>`, and then run `qsub submit.sh`. The rest will be handled automatically.
	- ...the entire run on your own machine that has GNU parallel installed, `cd <outdir>` and then run something like `parallel -j4 --memfree 4G ./run_model.sh {} :::: model_files.txt`, adjusting flags as needed for the resources you have available.


### Initial runs will be N stressed

I started some local test runs that have been going in the background while I fussed with the run scripts, and on examination the plants are still N-stressed even with a 2x multiplier on nitrate. Tried again with higher values, but even 10x only brings stress factor up to 0.86 and plant dry weight in day 40 to 68% of no-N plant weight (23.2 vs 34.1 g). Inspecting VTP shows that N concentration is always very low in the upper soil and depletion keeps pace with roots as they grow -- suspect there is a LOT of leaching in this model. As noted 2019-06-03, this probably means I should switch locations and simulate Rock Springs instead of Wageningen. But meanwhile, I'm going to start a full batch of simulations running with the Wageneningen parameters. They'll be N stressed, but will at least give me some initial results to work from.


### Run setup: axial penetration strength x lateral penetration strength x soil density

Using the version of `model_base.xml` from commit 16993f35, set up a run for the cluster:

```
./set_up_models.R whorl_impedance_wageningen_2xN_20190605 6
tar czf whorl_impedance_wageningen_2xN_20190605_inputs.tgz whorl_impedance_wageningen_2xN_20190605
scp whorl_impedance_wageningen_2xN_20190605_inputs.tgz datamgr.aci.ics.psu.edu:work
```

Next I need to compile a patched binary for the cluster:

```
ssh aci-i.aci.ics.psu.edu
cd ~/OpenSimRoot
module load git
git fetch --all
git checkout impedance-merge-rbih
git log -n1 # on commit 2cf16de1
cd OpenSimRoot/StaticBuild
module load gcc/7.3.1
make clean && make -j24 all
mv OpenSimRoot ~/bin/OSR_impedrbih_aci_linux_2cf16de1
```

Note to self: Did not actually need to include the OSR binary built for OS X in the tarball I copied to the Linux cluster! Now need to unpack the tarball and edit contents to point to the correct binary.

```
tar xzf whorl_impedance_wageningen_2xN_20190605_inputs.tgz
cd whorl_impedance_wageningen_2xN_20190605
ln -sf ~/bin/OSR_impedrbih_aci_linux_2cf16de1 OSR_impedrbih_2cf16de1
```

Also, I copied the run scripts from a project whose models ran MUCH faster, so they currently have PBS directives that request only 1 hour per model and 3 hours for submissions of the entire run. Test runs for the current models seem to take at least 2.5 hours, so changed times to 5 hours for `run_model.sh` and 12 hours for `submit.sh` by manually editing them in `vim` rather than remake the tarball.

Last (hopefully), need to set the scripts as executable and submit the batch. Logged out of ACI-i and logged back in to ACI-b, then:

```
cd work/whorl_impedance_wageningen_2xN_20190605
chmod +x run_model.sh submit.sh
qsub submit.sh
```


### Environment change: NL->PA

Let's switch environments from Wageningen to Rock Springs. Opened all the include files in `InputFiles/environments/Rocksprings` and manually copied values into `model_base.xml`. Notes as I worked:

* Many of these values seem suspiciously wrong for Rock Springs to me -- for example, `latitude` is set to 50.8 (which would be correct for Jülich) rather than 40.7 -- but copied them all as a starting point. TODO: check these all carefully, especially atmosphere file.
* Many other values are identical between Wageningen and Rocksprings files; I'm not sure if this is because they're defaults that ought to be site-independent or because they were measured in one place and then used elsewhere for lack of better numbers. TODO: include these in the careful checking.
* Nitrate concentration is actually the same at both locations! This makes me more confident changing other conditions is the correct decision. I reset the multiplier to 1.0 for a first pass, will adjust as needed after test-running.

Initial test run with reduced soil dimensions (26x26x100 instead of 26/60x150) and voxel resolution (2x2x2 instead of 1x1x1) fails with error message "MichaelisMenten: Fluxdensity is not a number. Debugging info: 0.243788 1.920000 nan 0.016100". Reading source code, appears the NaN is from `nutrientConcentrationAtTheRootSurface`, but not clear which nutrient was being calculated at failure time. Reset to full volume and resolution; next run completed without error.


### Run setup: lateral x axial x bulk density, Rock Springs evironment

* Set mode `+x` for repository copies of `run_model.sh` and `submit.sh`; hopefully this will save needing to reset them every time I copy to the server.
* Updated PBS headers to request 5 hours walltime for `run_model.sh` and 12 hours for `submit.sh`. Since the current run is 90 models (=probably they will usually all fit in one queueful), 12 hours is probably overkill, but I'd rather not have a run killed by forgetting to change it later.

Now to generate files:

```
./set_up_models.R whorl_impedance_rocksprings_20190605 6
rm whorl_impedance_rocksprings_20190605/OSR_impedrbih_2cf16de1
tar czf whorl_impedance_rocksprings_20190605_inputs.tgz whorl_impedance_rocksprings_20190605
scp whorl_impedance_rocksprings_20190605_inputs.tgz datamgr.aci.ics.psu.edu:work
```

```
ssh aci-b.aci.ics.psu.edu
cd work
tar xzf whorl_impedance_rocksprings_20190605_inputs.tgz
cd whorl_impedance_rocksprings_20190605
ln -sf ~/bin/OSR_impedrbih_aci_linux_2cf16de1 OSR_impedrbih_2cf16de1
qsub submit.sh
```

Submitted while about 85 of the 90 models from the Wageningen run were still going, so this will be a test of how well `submit.sh` works with an initially fulll queue!


## 2019-06-6

Status of runs submitted last night:

Wageningen:
	- 70 completed successfully (`grep --include='log_*' -lR 'Finalizing output.*OK' . | wc -l`)
	- 19 terminated by the scheduler (`grep --include='log_*' -lR 'SigTerm' . | wc -l`). Judging  by a quick skim of the emails from the scheduler, these were all walltime violations, but I'm not sure how to check that programmatically.
	- 1 (`all1k_grad_1`) is still in the queue in suspended state! My understanding is that this means it was pre-empted by a higher-priority job and has to wait to continue on the same node it started on.

Rock Springs:
	- 64 completed successfully
	- 4 ended with model failures (`grep --include='log_*' -lR 'FAIL' . | wc -l`). Reviewing logs, all 4 of these have error "Numerical problem: impedance = 0.000000 in /origin/environment/soil/soilPenetrationResistance", and all occur on either day 29 (`ax1k_lat4k_grad_5`, `all1k_pp_5`) or 30 (`all1k_grad_3`, `ax1k_lat4k_grad_3`).
	- 21 terminated by the scheduler.
	- 64+4+21 = 89, so one is missing... which one and why?

Let's play elimination to find the missing Rock Springs job: `grep --include='log_*' -LR -e 'output.*OK' -e 'SigTerm' .` ==> returns only `all4k_grad_6`; log ends at 36 days with no warnings (Jobs killed with SigTerm print warnings to the log before they exit). Searching my email for the job ID (12757320) shows it was killed after exceeding the 5-hour limit with messages that look the same as for the other killed jobs; not sure why this one didn't keep the OSR warnings.

How much more time do these runs need? Binning the models that finished by hours of runtime (rounding down).
	* Wageningen models: 1 took 1-2 hours, 23 2-3 hours, 50 3-4 hours, 14 4-5, and 1 finished in 5:00:10.
	* Rock Springs models: 5 took 1-2 hours, 67 2-3, 15 3-4, 2 4-5.
	* Oddly, I see no clear division in runtime between treatments; At both sites, all factors have all their levels present both the finished and timed-out sets.
	* timed-out set from both sites has *very approximately* the same distribution of root strengths (e.g. 1 default_roots, 1-2 baseline, 4-5 `ax1k_lat4k`, etc), but soil hardnesses and replica numbers don't align => whatever is making these models slow is *widespread* and probably *not* highly dependent on conditions within a particular replicate or RNG state.
	* How many days did the incomplete models finish before being killed? I spent a ridiculously long time figuring out how to extract this from the last date stamp in the log file, came up with this extremely ugly pipeline: "In all the log files. find daily progress lines that don't have `OK` in them, slice out the filename and last-printed day, and sort the lot"...
	```
	grep --include='log_*' -Ro '.0/40.0 days[^K]*$' . \
	| sed -n 's/\(.*\)\/log.*\([0-9]\{2\}\.0\)\/40.0 days.*\?$/\1 \2/p'\
	|sort -n -k2
	```
	...And then realized as I was typing this note that it would have been WAY faster and easier to look at the last line of each `tabled_output.tab`. But it's done now: Rock Springs models were killed in day 28-39, with about 1/3 of them in bins 28-33, 34-36, and 36-39. Wageningen models are less evenly distributed: still-suspended `all1k_grad_1` at day 11, 3 25-28, 7 30-32, 3 35-36, 6 38-39.
	* ==> Many of these models will need a LONG time to run to completion. Best to increase the walltime request by a very large amount rather than try to optimize it... and if models still time out then I'll figure out how to speed up the run rather than wait for them.

Resubmitting yesterday's jobs with a 12-hour walltime limit for each model:

* killed last running model: `qdel 12750399`
* Moved aside existing run directories:
```
cd ~/work
mv whorl_impedance_wageningen_2xN_20190605 whorl_impedance_wageningen_2xN_20190605_timeout1
mv whorl_impedance_rocksprings_20190605 whorl_impedance_rocksprings_20190605_timeout1
```
* re-expanded Wageningen input files, edited walltimes, changed script modes, and -resubmitted job as detailed yesterday. Only difference is that in `run_model.sh` I edited walltime to 12 hours instead of 5 and pmem up to 10 GB (probably not necessary, but might as well as long as I'm expecting big models...).
* re-expanded Rock Springs input files and re-linked binary as yesterday, manually edited `run_model.sh` to walltime of 12 hours and pmem of 1 GB, re-submitted as `qsub submit.sh`


## 2019-06-07

All models ran overnight and have exited. Checking status:

Wageningen:

* Directory contains 90 `roots040.00.vtp`, logs contain 90 `Finalizing.*OK` and zero `FAIL` ==> all models ran to completion.
* Memory usage: `grep --include='warnings.txt' -Ro 'mem.*'|sort -n -k4` ==> 1.6-3.1 GB
* Run time: Different reports from OSR's timer and the `$(time)` it ran inside.
	- OSR says run times ranged from 2:34:24 for `default_roots_grad_5` to 5:17:42 for `ax4k_lat1k_pp_6`: `grep --include='warnings.txt' -Ro 'Simulation took.*'|sort -n -k4` (N.B. sorting is on hours only; need to scan within the 2-hour category to find the shortest)
	- `time` says real process time ranged from minutes 154m18s = 2:34:18 for `default_roots_grad_5` to 454m32s = 7:34:32 for `all4k_unif_2`: `grep --include='log*' -Ro 'real\s*[0-9]*'|sort -n -k2`
	- for `all4k_unif_2`, `time` reports user time of 229m21s = 3:49:21 => agrees with OSR report. This model must have spent 4 hours waiting for system resources?
* Directory is 3.8 GB, compresses to 583 MB in 4:37

Rock Springs:

* Directory contains 86 `roots040.00.vtp`, logs contain 86 `Finalizing.*OK` and 4 FAIL -- same models as yesterday, failing on the same days with the same error message.
* Memory usage 1.6-5.0 GB
* Run time: OSR timer says 1:28:06 for `all1k_grad_3` up to 5:08:10 for `ax1k_lat4k_pp_6` (which is also the model with the highest memory usage) `time` says real 121-638 minutes = 2h1m to 10h38m!
* Directory is 4.2 GB, compresses to 637 MB in 4:14

Copied both back to laptop for further processing when I come back from vacation.


## 2019-06-19

### Plotting last week's output

Have been experimenting for past two day with approaches to plotting 2019-06-05 results; temporary plots are in `plots_20190617` and code is in `plot_draft 20190617.R`. Notable findings so far:

* Two models from the Rock Springs batch grow MANY more roots than the others: replicate 6 of `ax1k_lat4k_pp` has >75k cm of root where other replicates of the same treatment have 25-30k, and replicate 4 of `all4k_pp` has 60k cm of root where other replicates have around 40k. These plants are also larger than the others by other measures (dry weight, nutrient and water uptake, etc), though not as dramatically as the difference in root length.
* `ax1k_lat4k_pp_6` was also the slowest-running model that had the highest memory usage (unsurprising given all those roots), and it also has more C balance warnings... with the largest balance errors I've ever seen from a model that finished without error (they exceed 10% around day 30, then drop back to around 5% at the end).
* The outlier models get their root length by growing a dramatically *greater number* of lateral roots, not by growing more length of the same roots -- `ax1k_lat4k_pp_6` has almost 45000 laterals, `all4k_pp_5` has about 34000, and no other simulation produced more than 25k:
	```rs_all_40 %>% group_by(model_id, replicate, rootClassName) %>% summarize(n_root = n_distinct(root_number)) %>% ggplot(aes(model_id, n_root, color=replicate))+geom_point()+facet_wrap(~rootClassName, scales="free_x")+coord_flip()```
* Tracing carbon balance through time, both outliers behave like the other models up until around day 22, when they abruptly stop maintaining C balance and start racking up a carbon deficit. At this stage of the simulation C reserves are rising in most models, but both outliers keep reserves low and also have much more volatile nutrient stress factors than other plants. Overall impression: Somehow these two plants found a way to either escape or disregard a C limitation that applied to all the other plants with identical parameters but different RNG seeds. How? Still not sure.
* Considering genotype means instead of these two outliers, the effect of soil profile on plant size is largely consistent across genotypes: within a genotype, Total root length, dry weight, and lateral root number all generally follow the same pattern of uniform >= plowpan >= gradient. Exception: plants with weak axials (`all1k`, `ax1k_lat4k`) have slightly higher plant weight in gradient than plowpan, but their roots are still fewer and smaller in plowpan.
* Plants grown in uniform soil always have fewer brace roots than the same genotype in plowpan or gradient! Most apparent in RS (always 14 brace roots in uniform soft soil, 19-31 in impeded soils), but visible in Wageningen too

### Set up drought runs

I'm still not sure I've fully wrapped my head around the Wageningen vs Rock Springs differences, and I definitely don't understand the two high-lateral-length outliers or the four that failed with "impedance=0.0", but let's get the next round running while I think: I know I want to consider how these effects change with water availability, so rather than rerun the whole factorial I'll do a second Rock Springs run with the same 90 paramterizations (3 soils * 5 genotypes * 6 replicates), but with the soil drying from the top as growth proceeds. For a first step, I'll run the model with its default initial soil water content but no rainfall at all, and see how the plants do.

* Added one line to `factors_parameters.csv`, defining a factor named `precip` with only one level, `norain`. It sets `environment/atmosphere/precipitation` to "0 0  1000 0", i.e. no rain ever.
* Added a new `precip` column to `models_factors.csv`, set it to "norain" for all existing lines. Note again that I'm changing all the treatments and will compare them against the previously run output rather than defining new treatments; One I'm happy with the definition of the drought treatment, I imagine I'll want to add the default-precipitation runs back into the definition so they can all be regenerated at once.
* Generated a new set of six replicates: `./set_up_models.R whorl_impedance_rocksprings_norain_20190619 6`
* Tarred up, copied to cluster, and ran as documented for 20190605 runs

**N.B.** Not yet committing these changes to Git -- I expect this to take a few rounds of testing.


## 2019-06-20

Drought run seems to be incomplete:

* `submit.sh.o12902104` has 90 lines, so supposedly all 90 jobs were queued
* batch_log.txt contains 86 'start' lines and 84 'done' lines. Start but no end: `all1k_grad_6`, `ax1k_lat4k_pp_4`. Neither start nor end: `ax1k_lat4k_pp_6`, `ax1k_lat4k_unif_6`, `ax4k_lat1k_grad_6`, `ax4k_lat1k_pp_6`
* 86 `*.xml.o*` qsub logs in working directory, all empty as usual. The four missing models: `ax1k_lat4k_pp_6`, `ax1k_lat4k_unif_6`, `ax4k_lat1k_grad_6`, `ax4k_lat1k_pp_6` => same 4 with no start entry in batch log
* 86 log files inside run directories: `find . -name 'log*' | wc -l`
* Which 4 have no log file? `find . -mindepth 1 -type d '!' -exec sh -c "ls -1 {} | grep -q log*" ';' -print` => same 4 with no entries in batch log
* The fact that these are all from rep 6 seems relevant... In fact, these models are all listed contiguously (lines 83-86) in model_files.txt, so they would have been submitted sequentially by submit.sh, meaning I can indirectly guess they were *probably* assigned the job IDs from lines 83-86 of submit.sh.o12902104:
	```
	paste model_files.txt submit.sh.o12902104 |tail -n+83 | head -n4
	ax1k_lat4k_pp_6/ax1k_lat4k_pp_6.xml	12902205.torque01.util.production.int.aci.ics.psu.edu
	ax1k_lat4k_unif_6/ax1k_lat4k_unif_6.xml	12902206.torque01.util.production.int.aci.ics.psu.edu
	ax4k_lat1k_grad_6/ax4k_lat1k_grad_6.xml	12902207.torque01.util.production.int.aci.ics.psu.edu
	ax4k_lat1k_pp_6/ax4k_lat1k_pp_6.xml	12902208.torque01.util.production.int.aci.ics.psu.edu
	```
* Yep, there are no log outputs for these four jobs, e.g. `find . -name '*12902205*'` returns nothing but `find . -name '*12902209*'` finds `./ax4k_lat1k_unif_6.xml.o12902209`, the next model submitted after these disappearing 4.
* All 4 directories contain no output, just the input XML. If the models started running at all, they would have to have exited before OSR generated any files at all.
* Wait, I still have terminal scrollback! Reviewed the places I checked `qstat` before bed last night. Don't have timestamps, but I can see that:
	* On first check right after `submit.sh` started running, queue was 91 jobs long, with status of `submit.sh` "complete" and status of all 90 models "queued".
	* Jobnames of the missing models do line up with the job IDs I inferred from the line numbers. Whew.
	* On next check, the 4 missing jobs have already disappeared and the queue contains the remaining 86 models, all with status "running" and elapsed walltime of 46-48 minutes
	* I checked 3 more times over the course of the evening and saw models completing in more or less the expected times. At last check before bed, 8 models were still running with walltimes 3:17:x - 3:18:x. None of the models listed above here were among these eight.
* Emailed ICS helpdesk to ask what their logs say about this
* Reran the missing models:
	```
	tail -n+83 model_files.txt | head -n4 > model_files_retry.txt
	vim submit.sh # hand-edited line 13 to change MODEL_LIST from "model_files.txt" to "model_files_retry.txt"
	qsub submit.sh
	```

Of the 4 resubmitted models, 2 run to completion ( `ax1k_lat4k_pp_6`, `ax4k_lat1k_pp_6`), other two exceed their walltime limit and are killed in days 34 (`ax1k_lat4k_unif_6`) and 39 (`ax4k_lat1k_grad_6`). I don't want to wait again for these to rerun with longer walltime, so packed up all exiting norain output and copied back for analysis: `tar czf whorl_impedance_rocksprings_norain_20190619_outputs.tgz whorl_impedance_rocksprings_norain_20190619`

## 2019-06-24

Evaluated results of norain test in a hurry in between other tasks while traveling, did not take detailed notes. Bottom lines:

* Little change in biomass compared to normal precip schedule. In fact, plant weight and root length of highest-biomass treatments are about the same in both precip conditions, and in the lowest-biomass treatments they are slightly increased by no rain.
* The soil water content does not drop enough to impose a substantial difference in hardness or plant water status. Basically, Rock Springs soil is still pretty wet after 40 days of no rain.
* Seeing a few high-lateral-length outliers here too, not necessarily in the same treatments as before. Have discussed this briefly with other OSR developers; Postma thinks it's likely not an outright bug so much as a combination of random parameters that winds up making the observed biomass distribution be right-skewed. In short: These probably wouldn't be outliers if I were running more replicates.


Conclusions from norain run: To get a strong dry treatment, will need to reduce initiate water content or increase drainage/ET losses. But also: this implies I'll need to get water Actually Right, so first: Switch from the Hopmann water model to Doussan, both because it works better and laos it's what Ernst and Christian work with routinely, so they'll be happier to let them pick their brains about it :)


## 2019-06-30

### Converting to Doussan water model

Converting `model_base.xml` to use the Doussan module. As for site switch, my approach is to open the old and new include files (`plantTemplate.IncludeWaterModule` for Hopmanns, `plantTemplate.IncludeDrought` for Doussan), diff them, and manually copy the differences into `model_base.xml`. Why yes, yes this is tedious!

Doussan model requires a parameterization for the "lateral hydraulic conductivity" of each root class. This is poorly named (should probably be "axial hydraulic conductivity" -- it's conductivity along the root while "radial conductivity" is conductivity from outside of root to center). The public maize files do not include these, so using values from Ernst:

```
hypocotyl  0 0 10 4.32 100 4.32
primaryRoot 0 0 9 0.432 11 4.32 1000 4.32
braceroots 0 0 12 0.432 14 4.32 1000 4.32
braceroots2 0 0 12 0.432 14 4.32 1000 4.32
seminal 0 0 10.5 0.432 12.5 4.32 1000 4.32
nodalroots 0 0 11 0.432 13 4.32 1000 4.32
nodalroots2 0 0 11 0.432 13 4.32 1000 4.32
nodalroots3 0 0 11 0.432 13 4.32 1000 4.32
nodalroots4 0 0 11 0.432 13 4.32 1000 4.32
lateralOfCrownRoots 0 0.0 10 17.28e-6 12.5 6.48e-4 20 6.48e-4 25 17.28e-4 1000 17.28e-4
lateral 0 0.0 10 17.28e-6 12.5 6.48e-4 20 6.48e-4 25 17.28e-4 1000 17.28e-4
finelateral 0 0.0 10 17.28e-7 12.5 6.48e-5 20 6.48e-5 25 17.28e-5 1000 17.28e-5
finelateral2 0 0.0 10 17.28e-8 12.5 6.48e-6 20 6.48e-6 25 17.28e-6 1000 17.28e-6
```

Ernst's explanation of how he derived these values (copy-pasted from his LaTeX manuscript, markup preserved out of my laziness):

```
The axial hydraulic conductance of roots starts out low and increases as the root hydraulically matures. For maize, in the range between 15 and 140 mm behind the root apex, axial conductance was
 found to be constant, so xylem development and hydraulic maturing is not important yet here. Values were estimated from \cite{Frensch1989, Pierret2006}. \newline


\noindent From \cite{Pierret2006}, for main axes, axial (lateral) hydraulic conductivity goes from $0$ to $5\cdot10^{-10}\frac{m^4}{s\cdot MPa} = 0.432 \frac{cm^4}{d\cdot hPa}$ over the first
 35 cm from the apex and then to $5\cdot10^{-9}\frac{m^4}{s\cdot MPa} = 4.32 \frac{cm^4}{d\cdot hPa}$ at 45 cm from the apex. So we chose to set the axial (lateral) hydraulic conductivity to 0 0 9 0.432 11 4.32 100 4.32. Based on the growth rates, this takes
 about 10 to 14 days. Likewise, for the laterals, the axial (lateral) hydraulic conductivity goes from $0$ to $0.02\cdot 10^{-12} \frac{m^4}{s\cdot MPa} = 17.28\cdot10^{-6}\frac{cm^4}{d\cdot hPa}$ in 10 days, then to $0.75\cdot 10^{-12} \frac{m^4}{s\cdot MPa}
 = 6.48\cdot10^{-4}\frac{cm^4}{d\cdot hPa}$ in the next 2.5 days, then it stays at that level until day 20 and then increases to $2\cdot 10^{-12} \frac{m^4}{s\cdot MPa} = 17.28\cdot10^{-4}\frac{cm^4}{d\cdot hPa}$ in 5 days. So we chose to set the axial (radial)
 hydraulic conductivity to 0 0.0 10 17.28e-6 12.5 6.48e-4 20 6.48e-4 25 17.28e-4 100 17.28e-4. For each higher order of laterals we scaled this down by a factor 10.


Frensch1989 = 
Frensch, J. & Steudle, E. Axial and radial hydraulic resistance to roots of maize (Zea mays L.) Plant Physiology, Am Soc Plant Biol, 1989, 91, 719-726

Pierret2006 = 
Pierret, A.; Doussan, C. & Pages, L. Spatio-temporal variations in axial conductance of primary and first-order lateral roots of a maize crop as predicted by a model of the hydraulic architecture of root systems Plant and soil, Springer, 2006, 282, 117-126
```

In addition to changes specified by checking diffs between `plantTemplate.IncludeWaterModule` and `plantTemplate.IncludeDrought`, I also made five other changes:

* `/soil/water/relativeMassBalanceError` is specified in `plantTemplate.IncludeWaterModule` but not in `plantTemplate.IncludeDrought`, but I *did not* remove it. It seems useful for diagnostics, looks like it ought to work the same for both models, and my test runs don't complain when I leave it in.
* Removed `/hypocotylTemplate/growthpoint/rootWaterUptake` and `siblingRootTemplate/growthpoint/rootWaterUptake`. These are the only whole-root parameters specified inside growthpoints, `rootWaterUptake` only appears outside of the growthpoint in both water template files, no other public input files specify `rootWaterUptake` anywhere, and test runs don't complain when I remove them. ==> I think I must have added these in error in a previous manual edit of this input file.
* Added timestep specifications for SWMS: inserted `minTimeStep="0.0001" preferedTimeStep="0.001" maxTimeStep="0.2"`, all of which were previously unspecified, into the existing attributes for `/soil/Swms3d`. In a test run of 20 days in a 20x20x100 cm soil volume, this cuts run time from around 4.5 minutes ± 10 seconds to around 4 minutes ± 10 seconds; currently running a full-volume 40-day test as well, but my previous Doussan maize test runs have taken 10-12 hours, so it'll be a while before it finishes.
* Changed `bounceOfTheSide` from 0 to 1 for all root classes, to simulate root overlap in the field instead of pot-bound conditions with the root running along the edge of the simulation volume
* Divided existing single root length profile layer for all roots below 90 cm into six layers so whole profile down to 150 cm gets reported in 10 cm layers. Also added depth-specification variables `y1` and `y2` to `simulationControlParameters/table/skipTheseVariables` to avoid about 30 lines per day of redundant output.

### Faster binary

While testing water module changes, I also spent some time thinking about the C++ code for the root impedance module, and realized I spend a *lot* of processor time repeating calculations -- `ImpedanceGao::calculate` gets called by every root tip at every timepoint, and was calculating its net stress term by redoing a summation over the bulk density of all soil layers above the growthpoint every time... but in my current implementation, soil bulk density never changes through the whole simulation! I modified the code to calculate net stress of the whole soil profile once at startup, cache the result, and look up from it for all future calls. Running five replicates of a small test simulation (20 days of maize in a 20x20x100 cm volume) iwth RNG seed unset yields an impressive speedup for the same input run with the new binary: previous build `OSR_impedrbih_ab986a296a` took {592, 563, 491, 544, 448} = 528±58 seconds, new precaching build `OSR_precalcnetstress_ecdcb0a9` runs in {356, 336, 367, 309, 350} = 344±22 seconds... 35% faster!

(Yes, `OSR_impedrbih_ab986a296a` is newer than the `OSR_impedrbih_2cf16de1` I used in the last few whorl impedance runs -- I merged in some other bug fixes during last week's OSR developer hackathon)

## 2019-07-14

### Setting up dry-soil simulations

Attempting to create a set of simulations where the soil dries over time enough to make a visible difference in growth impedance. Simply turning off precipitation with existing Rock Springs soil conditions isn't enough -- after 40 days there's still plenty of water in the soil. To fix, started from partially dry conditions:

* Precipitation 0 for whole simulation
* Initial hydraulic head is -1000 kPa at surface rising to -200 at bottom of column (default is -215 rising to -10)
* Increased saturated flow rate at base of column -- current default if 10 cm/day  from surface to -140, then 1 cm/day below that. This seems very slow, especially since the few values available in SSURGO (1) seem more consistent with like 80-200 cm day in most layers, and (2) make it look like Ksat likely increases rather than decreases in the -150 cm range. But I'll be conservative for now: only changed in dry condition and only bumped 150-300 cm layer from 1 to 5 cm/day. TODO: Investigate models with higher Ksats.

These values derived from test runs over the past few days; did not keep many notes, was just looking for hard enough soil to see impedance rise without getting into unrealistically dry conditions. These conditions look good in small-pot test runs and for the first 10 days of ful-volume test; for full-scale test I'll turn it loose on all the models at once.

### Dry-soil test run

Only running the dry-soil simulations -- same as 2019-06-20, will compare them against existing wet output before setting up any new runs with both in them. But I'll throw in both and wet and a dry of the baseline model, just to check.

Before running `set_up_models.sh`, need to fix it for yet another dplyr API change: As of dplyr 0.8.1, `group_map` returns a list instead of a dataframe; I now want `group_modify` instead. Also fixed a couple of spacing typos while I was at it.

```
./set_up_models.R whorl_impedance_norain_20190714 6
rm whorl_impedance_norain_20190714/OSR_precalcnetstress_ecdcb0a9 #Should probably just make the script stop copying this in
tar czf whorl_impedance_norain_20190714_inputs.tgz whorl_impedance_norain_20190714
scp whorl_impedance_norain_20190714_inputs.tgz datamgr.aci.ics.psu.edu:work
```

```
ssh aci-b.aci.ics.psu.edu
cd OpenSimRoot
module load git
git fetch infotroph
git checkout precalc_netstress
git log -n1 # yep, got commit ecdcb0a9
module load gcc/7.3.1
cd OpenSimRoot/StaticBuild
make clean
make -j20 all
cp OpenSimRoot ~/bin/OSR_precalcnetstress_aci_linux_ecdcb0a9
cd ~/work
tar xzf whorl_impedance_norain_20190714_inputs.tgz
cd whorl_impedance_norain_20190714
ln -sf ~/bin/OSR_precalcnetstress_aci_linux_ecdcb0a9 OSR_precalcnetstress_ecdcb0a9
qsub submit.sh
```

Run started about 15:40 UTC+2 with job ID `13320833.torque01.util.production.int.aci.ics.psu.edu`

Checked in at midnight. None still running, 26 jobs were killed by the queue manager: I forgot to change the maximum runtime from 5 hours, which is apparently not long enough with Doussan water model enabled.

Edited `run_model.sh` to request 12 hours of walltime, generated new tarball of inputs, re-uploaded, re-expanded, re-ran. Run started at 0:38 UTC+2, process 13325531.


## 2019-07-15

101 of 102 models ran to completion; runtimes reported by `grep --include='log*' -R 'Simulation took' . | cut -d: -f1,5,6,7|sort -t: -k2 -n` range from from 1h44m (`all1k_unif_2`) to 9h33m (`all4k_pp_2`).

One model (`ax1k_lat4k_pp_1`) was killed in day 39; not sure why. Log just says `/var/spool/torque/mom_priv/jobs/13325540.torque01.util.production.int.aci.ics.psu.edu.SC: line 31: 122755 Killed                  ${OPENSIMROOT} ${PARAM_FILE}`. File timestamps suggest model was probably stuck for a long time before being killed: last write to `tabled_output.tab` and `coringData_00.000,00.000.tab` were at 22:00 UTC-4, last write to `log_1563143981.txt` was 01:07.

I speculate `ax1k_lat4k_pp_1` may have been killed for exceeding its memory allocation. Reported memory usage in day 39 was 2947 MB, (no other model used more than 2378 MB in day 39 or more than 2609 MB in day 40), and the increase from day 38 (2049 MB) to day 39 (2947 MB) was 898 MB where other models didn't add more than 150 MB:

```
grep --include='log*' -oER '38.0/40.0 days. Mem.*39.0/40.0 days. Mem ....' \
	| awk '{print $1,$10-$4}' \
	| sort -k2 -n \
	| tail
all4k_unif_6/log_1563144007.txt:38.0/40.0 112
all4k_pp_1/log_1563143978.txt:38.0/40.0 114
all4k_unif_5/log_1563144008.txt:38.0/40.0 114
all4k_pp_2/log_1563143981.txt:38.0/40.0 117
baseline_dry_6/log_1563144011.txt:38.0/40.0 122
baseline_dry_1/log_1563143986.txt:38.0/40.0 126
baseline_dry_5/log_1563144012.txt:38.0/40.0 140
ax1k_lat4k_unif_2/log_1563143997.txt:38.0/40.0 143
ax1k_lat4k_pp_2/log_1563143993.txt:38.0/40.0 147
ax1k_lat4k_pp_1/log_1563143981.txt:38.0/40.0 898
```

To exceed the 6 GB memory limit still requires another doubling of memory from day 39 to 40 where the other models increased by 18% at most (calculated with similar command as above, but grepping for days 39 and 40 and calculating ratio instead of difference: `awk '{print $1,$10/$4}'`). But the day 38-39 increase is 44% where other models didn't increase more than 8%, so that seems plausible.

The long hang time is also consistent with an out-of-memory condition. I've seen this in previous high-memory models and I don't fully understand what's happening, but what I've gathered from various HPC help pages is that PBS's implementation of memory limitation considers virtual memory separately from resident memory, meaning some (all?) combinations of node setup and job language can lead to the job using up its main memory allocation, switching to virtual memory (="swap") instead, and thrashing the disk until killed. But even if I'm right about that much, I don't know what triggers the killing when it's not an out-of-walltime event.

I could test this by rerunning the failed model with a higher memory limit, but I have output from at least 5/6 reps of every condition so let's move on to evaluating output instead.

My Internet connection seems unstable today (there's a festival in town and I guess the visitors are using all the bandwidth?), so didn't want to rely on keeping an SSH session open while compressing results. Submitted the tar command as a qsub-from-stdin:
```
cd ~/work
qsub -A open -lmem=12gb -lwalltime=4:00:00 -
cd ${PBS_O_WORKDIR}
tar czf whorl_impedance_norain_20190714_outputs.tgz whorl_impedance_norain_20190714
# ctrl-D to submit
```


## 2019-08-05

Finally looking at norain simulations run in mid-July. See plot_rain_vs_norain_20190805.R for code, but the key takeaway: I can't compare these dry outputs to pre-existing wet outputs because the wet outputs didn't use Doussan water! Baseline maize from wet run has 40-80% more root in each layer than `baseline_wet` maize from dry run, while wet and dry baseline runs with Doussan model only differ by 25ish percent ==> no, these really aren't comparable. Go back and run the dry and wet models properly in the same run, Chris.


## 2019-08-06

Running wet and dry models together, with some distractions on the way:

* Added wet rows to `factors_parameters.csv` by copying existing rows (excluding `baseline`, `baseline_wet`, `baseline_dry`), pasting below, changing `precip` column of pasted lines from "dry" to "wet", appending `_dry` or `_wet` to all `model_id`s.
* Because I can't leave well enough alone, also modified `model_base.xml` to turn off output of coring files. This only saves 8-10 KB per model, but it removes two output files that I'm fairly certain I'll never want to look at from this experiment.
* Edited `set_up_models.sh` to stop copying the Mac-compiler OSR binary into directories I'm about to copy over to the Linux cluster (just commented out the line for now, could do something fancier later).
* Added an epilogue script named `postjob.sh` that will be run by Torque when each model job exits. Doing this mainly in hopes of getting a better idea what resources were actually used by the job, and of recording exit status somewhere easier to find than the emails sent by the scheduler when it kills a job. Have tested this with non-OSR jobs on the cluster and discussed it with ACI help desk reps, but we'll see how it works in practice. N.B. this means the Torque output file from each model (e.g. `ax4k_lat1k_unif_4.xml.o12767956`) will no longer usually be empty.
* Moved `submit.sh`, `postjob.sh`, `run_model.sh` into a new directory `cluster_scripts`
* Increased memory allocation in `run_model.sh` from 6 to 12 GB, in hopes of avoiding the model crash seen from `ax1k_lat4k_pp_1` last time. TODO later: Test whether I can kill out-of-memory jobs immediately (rather than having them hang for ages and run out of walltime) by limiting virtual memory as well as real memory (possibly using `-l vmem=...` or `-l pvmem=...`?).  If out-of-memory jobs were killed right away I'd feel better about keeping the limit lower for most jobs and only raising it for the ones that were killed at a lower bound.

Okay, enough distraction, let's try to run this.

```
./set_up_models.R whorl_impedance_20190806 6
tar czf whorl_impedance_20190806_inputs.tgz whorl_impedance_20190806
scp whorl_impedance_20190806_inputs.tgz datamgr.aci.ics.psu.edu:work
```

```
ssh aci-b.aci.ics.psu.edu
cd ~/work
tar xzf whorl_impedance_20190806_inputs.tgz
cd whorl_impedance_20190806
ln -sf ~/bin/OSR_precalcnetstress_aci_linux_ecdcb0a9 OSR_precalcnetstress_ecdcb0a9
qsub submit.sh
```

Job queued at 07:12 PA time with job ID `13610146.torque01.util.production.int.aci.ics.psu.edu`, first 99 models running by 07:18.

09:32: 4 jobs have completed, but their Torque output files are empty ==> apparently either postrun script isn't running or output isn't being captured. Hypothesis: Maybe it's not running because `run_model` is submitted from a subdirectory and I should pass its path as `#PBS -l ../postrun.sh`? Will let this run finish without postrun scripts and investigate more for next time.


## 2019-08-07

All jobs have exited; checking run status.

* output directory contains 186 Torque log files `*.xml.o*`, all empty ==> All 186 models were submitted, and epilogue script didn't run for any of them.
* `submit.sh.o13610146` is 187 lines: line 1 says "tail: write error: Broken pipe", then 186 job IDs. No output from postjob.sh here either ==> maybe the thing keeping epilogues from running isn't a path issue, or at least not *entirely* a path issue.
* `grep 'done' batch_log.txt | sed 's/(//' | sort -n -k4` ==> run times range from 6829 to 43033 secs = 1.9 to 11.9 hours
* `grep --include='log*' -R 'mem usage' | sort -k7 -n` ==> memory usage ranged from 1721 to 3272 MB
* `grep -c start batch_log.txt` = 186, `grep -c done batch_log.txt` ==> 2 models either incomplete or failed to log their end
* Which two? `diff -u <(grep start batch_log.txt|cut -d' ' -f1|sort) <(grep done batch_log.txt | cut -d' ' -f1 | sort)` ==> `ax1k_lat4k_pp_dry_3.xml` and `ax1k_lat4k_pp_dry_5.xml`
* `grep --include='log*' -R 'FAIL' .` retuns nothing ==> no outright model failures
* `grep --include='log*' -R 'Finalizing output.*OK' .|wc -l` returns 184 ==> 2 models exited before completion but without an OSR failure
* Which two? `grep --include='log*' -LR 'Finalizing output.*OK' .` says `./ax1k_lat4k_pp_dry_5` and `./ax1k_lat4k_pp_dry_3/`, same as batch log does
* end of `./ax1k_lat4k_pp_dry_5/log_1565104632.txt`: `38.0/40.0 days. Mem 6442.0 mB. #obj.=1822015 x64b/obj.=463.5ESC[u/var/spool/torque/mom_priv/jobs/13611121.torque01.util.production.int.aci.ics.psu.edu.SC: line 32: 39421 Killed                  ${OPENSIMROOT} ${PARAM_FILE}`
* end of `./ax1k_lat4k_pp_dry_3/log_1565090290.txt`: `38.0/40.0 days. Mem 2559.0 mB. #obj.=1781583 x64b/obj.=188.3ESC[u/var/spool/torque/mom_priv/jobs/13610229.torque01.util.production.int.aci.ics.psu.edu.SC: line 32: 118790 Killed                  ${OPENSIMROOT} ${PARAM_FILE}`
* Looking at file timestamps in `ax1k_lat4k_pp_dry_3/`: Last write to modelDump000.00.xml at 07:19, roots030.00.vtp at 08:31, tabled output at 10:57 (last line of output is from day 38), `log_1565090290.txt` at 17:09 (see last line above) ==> model ran to day 30 in about an hour, to day 38 in about 1.5 hours more, then hung for 8.5 hours in day 39 and was killed when it ran out of walltime at hour 12.
* Ditto in ax1k_lat4k_pp_dry_5: Last write to model dump at 11:18, roots 30 at 12:34, tabled output at 16:42 (last line from day 38), log at 22:03 ==> same story here, with very similar timing. Sure wish I had some epilogue script output to look at...

I'm puzzled why these two models stalled out without, as far as I can tell, running out of memory or hitting any other obvious errors. The four other replicates of model `ax1k_lat4k_pp_dry` ran in 3.2-4.3 hours using 2126-2469 MB of memory, neither of which is exceptional compared to the rest of the run.

In any case, I can't think of any other checks to do on the cluster, and don't want to wait for any attempted reruns of the failed models. Let's package this up (whole output directory is 7.2 GB, compresses to 1.2 GB) and copy it back for local analysis.

```
qsub -A open -lmem=12gb -lwalltime=4:00:00 -
cd ${PBS_O_WORKDIR}
tar czf whorl_impedance_20190806_outputs.tgz whorl_impedance_20190806
# ctrl-D to submit
```


* * *

Thinking more about why epilogue didn't run: My path error hypothesis doesn't make sense because `run_model.sh` is submitted from the top level and changes directory after it starts. The Torque documentation says per-job epilogue script paths can be either absolute or relative to the submission directory, so the path I have should be OK.

Did some further testing in another terminal, details not saved. The upshot: Oh, right, `postrun.sh` didn't run because I didn't set it executable by owner. For next run, will try same scripts but with a `chmod +x postrun.sh` first.


## 2019-08-08

Making exploratory plots of 2019-08-06 outputs, putting code in `plot_20190806.R` at least for now (TODO: move away from completely rewriting my plotting code for every run!).

Most interesting finding so far: The two `ax1k_lat4k_pp_dry` models that didn't finish running appear to have encountered some kind of numeric problem in the water model -- in days 35ish-38 of the failed models only, water uptake rises dramatically while collar potential falls sharply. Reported nitrate availability also skyrockets by at least 12 orders of magnitude and in one model the reported depth to 95% of nitrate in the soil column jumps from 134 cm in day 37 to 2 cm(!) in day 38. Currently consulting with Ernst on possible causes; working hypothesis right now is that the surface soil layers became extremely dry and caused the water module to get stuck, while simultaneously forcing N solute "concentrations" to skyrocket because there's only a tiny amount of water ==> concentration is translated into total N in the layer => surface N goes through the roof ==> D95 of nitrate gets very shallow.

To debug, will rerun the `ax1k_lat4k_pp_dry` models on my computer and see whether they hang in day 39 and whether they show the same behavior:

* `mkdir ppdry_20190806_debug && cd ppdry_20190806_debug`
* Extracted fresh copies of the `ax1k_lat4k_pp_dry` inputs from tarball, renaming to avoid adding an intermediate `whorl_impedance_20190806` directory: `tar xzf ../whorl_impedance_20190806_inputs.tgz -s '/whorl_impedance_20190806//' '**ax1k_lat4k_pp_dry*'`
* For easier debugging, manually edited all 6 input files to turn on daily VTU output of both roots and soil by setting `run` from 0 to 1 and `timeInterval` from 10 to 1. Note that VTP files are still written every 10 days as before.
* launched jobs manually by opening 6 new terminals (I want to watch these all run anyway) and typing `cd <modelname>; ~/OSR_bin_archive/OSR_precalcnetstress_ecdcb0a9 <modelname>.xml 2>&1 | tee log.txt` in each. Running in 2 batches of 3: first reps 1-3, then will run reps 3-5 after that.
* When rep 1 reached day 20, tried to do a ctrl-C to see any warnings accumulated so far. Instead, go "illegal hardware instruction" and model exited immediately. Cursed, restarted model, will not plan to use ctrl-C for info from the other models unless they're very clearly hung.


First three replicates finish with no error in 3.75-4 hours, including rep 3 that failed on the cluster. Same warning messages from all 3 repeated similar numbers of times. Quick look at output VTUs shows rep 3 has a very small, concentrated ultra-dry zone around the crown of the plant -- looks like probably just the soil voxels in direct contact with the hypocotyl. Does not extend deep at all either. Reps 1 and 2 are discernably dryer near stem than in corners, but not nearly as pronounced.

Started reps 4-6 on the local machine, then set out to rerun on cluster as well:

```
ssh aci-b.aci.ics.psu.edu
cd work
mkdir whorl_impedance_20190806_ppfail_debug
cd whorl_impedance_20190806_ppfail_debug

# Extract inputs
# Caution: can't use the `-s '/whorl_impedance_20190806//'` that I used earlier today -- that's OS X only!
# Apparently on Linux tar, `-s` means `--same-order`.
tar xzf ../whorl_impedance_20190806_inputs.tgz --strip-components=1 '**ax1k_lat4k_pp_dry*' '**.sh' '**.txt'

# truncate model_files.txt so only these 6 models run
sed -i '/ax1k_lat4k_pp_dry.*/p;d' model_files.txt

chmod +x postjob.sh
qsub submit.sh
```

I did not modify input files at all -- theoretically this is an identical rerun of the models I ran before, just in their own directory and with the postrun script (hopefully) working. If I can reproduce the failures here, next step is to turn on VTU output and see if that changes the result. Job submitted around 22:48 NL time, job ID `13631335.torque01.util.production.int.aci.ics.psu.edu`


## 2019-08-09

### Cluster debug run errored

...Because I forgot to put a link to the OSR binary where run_model.sh could find it. Fixed that (`cp ../whorl_impedance_20190806/OSR_precalcnetstress_ecdcb0a9 .`), restarted at 08:56 NL time with job ID  13635701.torque01.util.production.int.aci.ics.psu.edu.

While I'm at it, let's run it in triplicate so I can see whether any failures are consistently reproducible on the same hardware: Copied whole directory 2x as `whorl_impedance_20190806_ppfail_debug2` and `whorl_impedance_20190806_ppfail_debug3`, removed run outputs already accumulated, started fresh runs in each at 10:10 (=04:10 cluster time). debug2 job ID 13635758, debug3 13635759.

### Local debug run

Replicates 4 and 5 completed overnight in about 3.5 hours. Replicate 6 is still working on day 40 and is probably hung:

* Last write to tabled output was day 39 at 01:18, log file reports 01:19.
* FEM VTU write times: day 38 12:16, day 39 12:53, day 40 02:34.
* root VTU write times: day 38 12:40, day 39 1:19, day 40 not written yet. Note the long lags between FEM and VTU write times for the same simulation day!
* `top` reports process is now at 12 hours of CPU time with 35GB total memory usage (yes, 35! Yes, GB!)
* Examining day 40 FEM files, all 6 models have much lower hydraulic head in the few voxels surrounding the plant crown, but in the 5 that completed "much" lower is on the order of negative hundreds of kPa in the bulk soil and negative thousands near the crown. Replicate six has negative hundreds in the bulk soil and a minimum of -4.5e10 near the crown!

I'm inclined to take the fact that the FEM file is written successfully before the model hangs for hours as a clue: Could still be absurdly-low soil water numbers that drive the hang, but it looks to me as if the hang happens when some other component (root growth model?) tries to process those values. And, judging by swap usage, that component tries to allocate some VERY large objects in the process.

Will let replicate 6 keep grinding away for a while before I interrupt it. If it's not done after lunch, will try sending an interrupt to check for errors, then kill the job if the interrupt doesn't kill it for me.

.. It finished! Log file, tabled output, and roots040.00.vtu all updated at 11:12. At 11:30 (when I looked over and noticed update), process was still active with 38 GB memory used, around 15-50% CPU, and state "stuck". Console has reported day 40 memory usage (5229 MB) and "OK" to indicate end of simulation, but not yet printed "finalizing output" or written warnings.txt. CPU time at 11:43 is 13:34:xx (neglected to record at 11:30).

* 13:20: total memory now rounds off to 37 GB, compressed to 34 GB
* 14:56: cpu time is 14:37:01, mem 36, 33
* (wall time not recorded): cpu time 14:41:34, 36, 33
* (wall time not recorded): cpu time 14:46:22, 35, 32
* (wall time not recorded): cpu time 15:05:55, 30, 27
* (wall time not recorded): cpu time 15:13:41,  29, 26
* 17:04: 15:29:54, 26, 22
* 17:11: 15:33:38, 24, 21
* 17:20: 15:40:xx, total memory drops below 10 GB and keeps dropping faster than I could type
* 17:21: warnings.txt written and fem.pvd written, process exits

While waiting, I took periodic samples of the process using OS X's Activity Monitor GUI (right-click on process, choose "Sample Process") to get an idea what calls were happening. On every check, the entire call tree consisted of nested calls to the `SimulaBase()` and `~Database()` destructors ==> During this long hang after reporting model success, OSR is busy destroying objects before exiting.


## 2019-08-15

copied cluster results back to local machine, compared between reruns: `diff -ur whorl_impedance_20190806_ppfail_debug whorl_impedance_20190806_ppfail_debug3` to compare identically-named files, then a manual series of e.g. `git diff --no-index --word-diff=color --word-diff-regex=. whorl_impedance_20190806_ppfail_debug/ax1k_lat4k_pp_dry_6/log_1565333888.txt whorl_impedance_20190806_ppfail_debug3/ax1k_lat4k_pp_dry_6/log_1565338317.txt` to compare the differently-named log files (using `git diff` instead of plain unix `diff` because only a few characters differ per long output line, so I'm asking git diff to color-code the differences for me).

Only differences are a few seconds variation in total runtime, and a few rare rounding differences in the `x64b/obj` field of the daily log output. Total memory usage and number of objects each day is identical between reruns ==> Yes, this bug is reproducible with the same random seed on the same hardware.


Trying to reduce runtime needed to reproduce: Can I induce the hang in a smaller (=faster-running) environment? Copied locally-hanging rep 6 to a new directory `ax1k_lat4k_pp_dry_6_smallpot`, edited soil volume to 26 x 26 surface and 50 cm deep.

* Run started 13:19
* FEM and root files within ~1 minute of each other through day 25
* day 26: fem written 13:31, root 13:34
* day 27: fem 13:36, roots 13:41
* day 28: fem 13:44, roots 13:51
* day 29:
	- table 13:47? Looked once, opened in Sublime Text, accidentally hit save and disturbed timestamp so can't double-check.
	- fem 13:56
	- 15:12: memory has risen from ~2  GB to >11, roots not yet written.
	- 15:16: Sampling process shows `SWMS3d::calculate` active, mostly calling `watflow::calculate` and `VanGenuchten::calc_fc`
	- root 15:48
	- 15:54: memory 13.4 GB, sampling process shows all time is spent in `soute::calculate` >  `BiCGSTAB::solve` > `SparseMatrix::vectorMultiply`
	- log reports memory for day 29 as 5596 MB.
* 16:02: Model fails and exits with error `Watflow:: h=4.39818e+13 at 29.6013`.

That error message comes from `Watflow::calculate` and is issued whenever `hOld.max()` is greater than 0. Float underflow?

## 2019-08-19

Notes from a conversation with JPL that I jotted into a separate file and forgot about; copied back here 2021-05-22:

* Topline result from 20190806 simulations: root length density near surface is ~fully controlled by lateral strength, rooting depth ~fully controlled by axial strength. Even in plowpan -- 1k4k and 4k1k converge in highest-density layer then separate again!
* dry-grown plants are slightly smaller than wet-grown across the board, and the difference is generally a bit larger in strong-axial than weak-axial plants
* gradient density is slightly N-stressed under all conditions. Not sure why -- we first guessed higher leaching, but doesn't look like it. After meeting CKB notes max root depth of gradient is always 10s of cm less than others, so this is probably related tothat somewhow
* When scanning the hideous figure of all varaiables that change, main point to note is that most show up-and-right pattern indicating (magnitude of water effect) >= (magnitude of axial effect) > (magnitude of lateral effect). Exception: root length, where lateral effect is stronger than axial effect. This isn't surprising (most root length in the plant is lateral), but it is notable that resource uptake doesn't follow it.
* Debugging of water model crashes leads to an interesting observation: models with lateral roots confined above plowpan achieve VERY dry soil in an increasingly-dry sphere around the crown of the plant. Below c. 1000 kPa, root water uptake rate is negative --> hydraulic lift! Only in a small band in this case (soil is too wet outside it and too dry inside), but CKB has looked at other model runs where it's more prevalent. N.B. Would need to check whether this negative flow actually results in any water being added to the matching FEM cell.
* Agreed this is a paper's worth: Have implemented a physics-based representation of how root elongation responds to soil hardness, and used it to explore the potential utility of adjusting penetration ability as a breeding target for deeper-rooted plants. Our conclusion: If we were able to breed for a tradeoff between the penetration ability of axial and lateral roots, taking the strong-axial side of the tradeoff is likely to be a winning proposition.
* JPL adds: In monocots. Given how fully maize roots are one-axial-bearing-many-small-laterals, it makes sense that you miss more opportunities by losing axial growth than by losing lateral growth even if most of the total root length is laterals. To note in the discussion but not explicitly test here: We might expect that for dicots with multiple orders of laterals-from-laterals, we might expect bean etc to suffer proportionally more when its laterals are impeded.
* CKB wants to make sure he can make a compelling case why it's biologically reasonable to expect axials and laterals to behave differently with respect to penetration ability. JPL says it's probably sufficient to invoke diameter differences plus some anatomy. Rather than doing my own lit review, JPL recommends I ask to poach references from Dorien.
* Also for a future paper and not this one: Consider developmental changes (e.g. differences in penetration ability between successive whorls of the same root class).


## 2020-04-12

Updated model_base.xml to incorporate fix for [OpenSimRoot bug 43](https://gitlab.com/rootmodels/OpenSimRoot/-/issues/43): `//plantTemplate/stressFactor:impactOn:rootPotentialLongitudinalGrowthMultiplier` was intended to be the factor that controls how much nutrient stress changes root growth rates, but it's the wrong name: the nutrient stress module actually looks for `//plantTemplate/stressFactor:impactOn:rootPotentialLongitudinalGrowth`, so Ernst reports that root growth rates were not being reduced. Solution: Delete the `Multiplier` from the end of the name.

